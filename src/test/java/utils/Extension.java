package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Extension {
    public ExtentHtmlReporter htmlReporter;
    public static ExtentReports extent;
    public static ExtentTest test;
    public static AppiumDriver<MobileElement> driver;
    @BeforeTest
    public synchronized void setExtent(){

        String dateToday = new SimpleDateFormat("yyyy MM dd hh mm").format(new Date());

        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/extent/Report"+dateToday+"/Report from "+dateToday+".html");
        htmlReporter.config().setDocumentTitle("Automation Report");
        htmlReporter.config().setReportName("Drigster");
        htmlReporter.config().setAutoCreateRelativePathMedia(true);
        htmlReporter.config().setCSS(".r-img {width: 60%}");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        // Information général
        extent.setSystemInfo("Host name", "localhost");
        extent.setSystemInfo("Environemnt", "QA");
        extent.setSystemInfo("user", "Rojo");
//
        try {
            //initialisation Devices Android/IOS => App or browser
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability(CapabilityType.PLATFORM_NAME, "Android");
            caps.setCapability(CapabilityType.VERSION, "6.0");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Orange Rise32");
            caps.setCapability(MobileCapabilityType.UDID, "02598107BG000617");
            //caps.setCapability(MobileCapabilityType.APPLICATION_NAME, "");
            caps.setCapability(MobileCapabilityType.APP, "com.sapheerbank");

            //caps.setCapability(MobileCapabilityType.BROWSER_NAME, "");
            //caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);

            URL url = new URL("http://localhost:4723/wd/hub");
            driver = new AppiumDriver<MobileElement>(url, caps);
        }catch (Exception e){
            System.out.println("ERROR cause : "+e.getCause());
            System.out.println("ERROR Message : "+e.getMessage());
            e.printStackTrace();
        }




        // For web app
        /*
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("");
        */
        //for Mobile devices Utilisation de Desired Capability 


    }

    @AfterSuite
    public void endReport() {
        extent.flush();
    }


    @AfterTest
    public void teardown(){
        driver.close();
        driver.quit();
    }
    @AfterMethod
    public synchronized void listenerMeth(ITestResult result) throws IOException {
        if (result.getStatus() == ITestResult.FAILURE) {
            test.log(Status.FAIL, MarkupHelper.createLabel(" FAILED ", ExtentColor.RED));
            test.fail("Cause : "+result.getThrowable());
            String screenshotPath = getScreenshot(driver, result.getName());
            test.addScreenCaptureFromPath(screenshotPath);

        } else if (result.getStatus() == ITestResult.SKIP) {
            test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
        } else if (result.getStatus() == ITestResult.SUCCESS) {
            test.log(Status.PASS, "Comportement Attendu :  <b style='font-size:18px; color: green;'>" + result.isSuccess()+"</b>");
            String screenshotPath = getScreenshot(driver, result.getName());
            test.addScreenCaptureFromPath(screenshotPath);
        }

    }
    public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
        String dateName = new SimpleDateFormat("yyyyMMddhhmm").format(new Date());
        TakesScreenshot ts = (TakesScreenshot) driver;
        String dateToday = new SimpleDateFormat("yyyy MM dd hh mm").format(new Date());
        File source = ts.getScreenshotAs(OutputType.FILE);

        // after execution, you could see a folder "FailedTestsScreenshots" under src folder
        String destination = System.getProperty("user.dir") + "/test-output/extent/Report"+dateToday+"/screenshots/" + screenshotName + dateName + ".jpg";
        File finalDestination = new File(destination);
        FileUtils.copyFile(source, finalDestination);

        return destination;
    }

}
