package apps.digster;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import utils.Extension;

public class LogPage extends Extension {
    public LogPage(AppiumDriver<MobileElement> driver){
        Extension.driver = driver;
    }

    MobileElement Connect = driver.findElement(By.xpath("//*[@text='Connexion']"));
    /*        MobileElement Login = driver.findElement(By.xpath("//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@class='android.widget.Button']]"));

    MobileElement LoginButton = driver.findElement(By.xpath(""));*/
    MobileElement Log = driver.findElement(By.xpath("//*[@text and @class='android.widget.EditText']"));
    MobileElement Password = driver.findElement(By.xpath("//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@class='android.widget.Button']]"));

    public void setConnect(){

        Connect.click();
    }
    public void setLogin(String lgn){

        Log.sendKeys(lgn);
    }
    public void setPassword(String pwd){

        Password.sendKeys(pwd);
    }
    /*public void setLoginButton(){

        LoginButton.click();
    }*/
}
