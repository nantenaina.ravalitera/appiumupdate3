package apps.digster;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import utils.Extension;

public class CreateTransaction extends Extension {
    public CreateTransaction(AppiumDriver<MobileElement> driver){
        Extension.driver = driver;
    }

    MobileElement TransactionsList = driver.findElement(By.xpath(""));
    MobileElement NewTransaction = driver.findElement(By.xpath(""));
    MobileElement SelectBen = driver.findElement(By.xpath(""));
    MobileElement IntBen = driver.findElement(By.xpath(""));
    MobileElement NextButton = driver.findElement(By.xpath(""));
    MobileElement Title = driver.findElement(By.xpath(""));
    MobileElement Amount = driver.findElement(By.xpath(""));
    MobileElement Category = driver.findElement(By.xpath(""));
    MobileElement ValidButton = driver.findElement(By.xpath(""));

    public void setTransactionsList(){
        TransactionsList.click();
    }

    public void setNewTransaction(){
        NewTransaction.click();
    }
    public void setSelectBen(){
        SelectBen.click();
    }
    public void setIntBen(){
        IntBen.click();
    }
    public void setNextButton(){
        NextButton.click();
    }
    public void setTitle(String tle){
        Title.sendKeys(tle);
    }
    public void setAmount(String am){
        Amount.sendKeys(am);
    }
    public void setCategory(){
        Category.click();
    }
    public void setValidButton(){
        ValidButton.click();
    }


}
